﻿board = true;
$("#game").on("click", "li", choiseCell);

function choiseCell() {
    if (board) {
        board = false;
        id = $(this).find("p").attr("id");
        updateBorderGame(id);
    }
}

$("#level").change(function () {
    board = true;
    clearTimeout(interval);
    val = $("#level option:selected").val();
    $.get('/Game/SetLevelAI', { level: val }, function (ajaxDate) {
        $('#game').empty();
        $('#game').append(ajaxDate);
    });
});
$("#restart").on("click", function () {
    $("#level").change();
});

$("#x").on("click", function () {
    choiseDice(true);
});

$("#o").on("click", function () {
    choiseDice(false);
});

function choiseDice(dice) {
    board = true;
    clearTimeout(interval);
    $.get('/Game/ChoiseDice', { dice: dice }, function (ajaxDate) {
        $('#game').empty();
        $('#game').append(ajaxDate);
    });
}

function updateBorderGame(tabIndex) {
    $.get('/Game/Move', { id: tabIndex }, function (ajaxDate) {
        $('#game').empty();
        $('#game').append(ajaxDate);
    }).done(function () {
        getStateGame();
    });
   
}

function getStateGame() {
    $.when(
         $.get('/Game/GetStateGame', function (ajaxDate) {
             $('#score1').empty();
             $('#score2').empty();

             $('#score1').append(ajaxDate.ScoreX);
             $('#score2').append(ajaxDate.ScoreY);
             $("#message h2").html(ajaxDate.Title)
             if (ajaxDate.TurnHuman) {
                 board = true;
             }
             else {
                 interval = setTimeout(function () {
                     updateBorderGame();
                 }, 1000);
             }
         })
        );
}

