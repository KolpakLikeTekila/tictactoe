﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TicTacToe.Models.TicTacToeGame;
using TicTacToe.Models.TicTacToeGame.Users;

namespace TicTacToeGame.Controllers
{
    public class GameController : Controller
    {
        TTT ttt = null;
        /// <summary>
        /// изменяет состояние игры
        /// </summary>
        /// <param name="position">ход игрока</param>
        private void ContinPlay(int? position)
        {
            Player playerCurrent = ttt.Game.CurrentPlayer;
            (playerCurrent as Human)?.ChoseCell((int)position);
            playerCurrent.Move(ttt.Game.Board);
            if (ttt.Game.GameOver)
            {
                if (ttt.Game.Board.CheckWinner(playerCurrent.PlayerContent))
                {
                    ttt.Score.Title = "Победил " + playerCurrent.Name;
                    if (playerCurrent.PlayerContent == Board.StateCell.X)
                    {
                        ttt.Score.ScoreX++;
                    }
                    else ttt.Score.ScoreY++;
                }
                else
                {
                    ttt.Score.ScoreDraw++;
                    ttt.Score.Title = "Ничья";
                }
                ttt.Score.TurnHuman = true;
            }
            else
            {
                ttt.Score.Title = "Ходит " + ttt.Game.CurrentPlayer.Name;
                ttt.Score.TurnHuman = ttt.Game.CurrentPlayer is Human;
            }
           
        }
        /// <summary>
        /// изменяет уровень слоности компа
        /// </summary>
        /// <param name="level"> уровень сложности</param>
        /// <returns>игровое поле</returns>
        public PartialViewResult SetLevelAi(int level)
        {
            InitTTT();
            Player curPlayer = ttt.Game.CurrentPlayer;
            if (curPlayer is Human)
            {
                if(curPlayer.PlayerContent == Board.StateCell.X)
                    ttt.Game = new Game(new Human("X", Board.StateCell.X), new Computer("O", Board.StateCell.O, level));
                else
                    ttt.Game = new Game(new Computer("X", Board.StateCell.X, level), new Human("O", Board.StateCell.O));
            }
            else
            {
                if (curPlayer.PlayerContent == Board.StateCell.X)
                    ttt.Game = new Game(new Computer("X", Board.StateCell.X, level), new Human("O", Board.StateCell.O));
                else
                    ttt.Game = new Game(new Human("X", Board.StateCell.X), new Computer("O", Board.StateCell.O, level));
            }
            ttt.Score.Title = "Ходит " + ttt.Game.CurrentPlayer.Name;
            Session["TicTacToe"] = ttt;
            return PartialView("BoardDraw", ttt.Game.Board);
        }
        /// <summary>
        /// изменяет очеррдность ходов
        /// </summary>
        /// <param name="dice">true - первый хоит чел</param>
        /// <returns>игровое поле</returns>
        public PartialViewResult ChoiseDice(bool dice)
        {
            InitTTT();
            if (dice)
                ttt.Game = new Game();
            else
                ttt.Game = new Game(new Computer("X", Board.StateCell.X), new Human("O", Board.StateCell.O));
            ttt.Score.Title = "Ходит " + ttt.Game.CurrentPlayer.Name;
            Session["TicTacToe"] = ttt;
            return PartialView("BoardDraw", ttt.Game.Board);
        }
        /// <summary>
        /// обробатывает ход игрока
        /// </summary>
        /// <param name="id">ход игрока</param>
        /// <returns>игровое поле</returns>
        public PartialViewResult Move(int? id)
        {
            InitTTT();
            if (ttt.Game.GameOver)
            {
                ttt.Game.NewGAme();
            }
            else
            {
                ContinPlay(id);
            }
                Session["TicTacToe"] = ttt;
            return PartialView("BoardDraw", ttt.Game.Board);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns>счет игры</returns>
        public JsonResult GetStateGame()
        {
            InitTTT();
            return Json(ttt.Score, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 
        /// </summary>
        private void InitTTT()
        {
            if (Session["TicTacToe"] == null)
            {
                ttt = new TTT();
                ttt.Score.Title = "Ходит " + ttt.Game.CurrentPlayer.Name;
            }
            else
                ttt = (TTT)Session["TicTacToe"];
        }

        // GET: Game
        public ActionResult Index()
        {
            ttt = new TTT();
            ttt.Score.Title = "Ходит " + ttt.Game.CurrentPlayer.Name;
            Session["TicTacToe"] = ttt;
            return View(ttt.Game.Board);
        }


    }
}