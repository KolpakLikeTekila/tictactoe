﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TicTacToe.Models.TicTacToeGame;

namespace TicTacToe.Helpers
{
    public static class PageHelper
    {
        /// <summary>
        ///  
        /// </summary>
        /// <param name="winMove"> очередь выйгрышных ячеек</param>
        /// <returns>позиция выйгрышой ячейки</returns>
        private static int indexWinPsotion(Queue<int> winMove)
        {
            if (winMove?.Count() > 0)
                return winMove.Dequeue();
            return -1;
        }
        /// <summary>
        /// рисует игровое поле
        /// </summary>
        /// <param name="html"></param>
        /// <param name="board">игровое поле</param>
        /// <returns>игровое поле</returns>
        public static MvcHtmlString PaintBroad(this HtmlHelper html, Board board)
        {
            int winPos = indexWinPsotion(board.winMove);
            TagBuilder ul = new TagBuilder("ul");
            for (int i=0; i< board.Size; i++)
            {
                TagBuilder li = new TagBuilder("li");
                TagBuilder div = new TagBuilder("div");
                div.AddCssClass("cell");
                TagBuilder div2 = new TagBuilder("div");
                div2.AddCssClass ("cell-content");
                if(board.Size-board.LenghtField > i)
                {
                    div2.AddCssClass("border-bottom");
                }
                if ((i+1)% board.LenghtField != 0)
                {
                    div2.AddCssClass("border-right");
                }
                TagBuilder p = new TagBuilder("p");
                p.MergeAttribute("id", i.ToString());
                if (winPos >= 0)
                {
                    if (winPos == i)
                    {
                        winPos = indexWinPsotion(board.winMove);
                        p.AddCssClass("winner");
                    }
                }
                if (board.GetBoardCell(i) == Board.StateCell.X)
                {
                    p.SetInnerText("X");
                    p.AddCssClass("player1");
                }
                else if (board.GetBoardCell(i) == Board.StateCell.O)
                {
                    p.SetInnerText("O");
                    p.AddCssClass("player2");
                }
                div2.InnerHtml += p.ToString();
                div.InnerHtml += div2.ToString();
                li.InnerHtml += div.ToString();
                ul.InnerHtml += li.ToString();
            }
            return new MvcHtmlString(ul.ToString());
        }
    }
}