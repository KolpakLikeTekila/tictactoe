﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicTacToe.Models.DB;
using TicTacToe.Models.TicTacToeGame.AI;
using TicTacToe.Models.TicTacToeGame.Users;

namespace TicTacToe.Models.TicTacToeGame
{
    public class Game
    {
        public Board Board { get; private set; } = new Board();
        private List<Move> Moves { get; } = new List<Move>();
        public Player CurrentPlayer { get; private set; }
        private List<Player> playersList = new List<Player>();
        public bool GameOver { get; private set; } = false;
        private DBRepos db = new DBRepos();

        public Game() : this(new Human("X", Board.StateCell.X), new Computer("O", Board.StateCell.O)) { }

        public Game(Player player1, Player player2)
        {
            playersList.AddRange(new List<Player>() { player1, player2 });
            CurrentPlayer = player1;

            player1.MoveHandlerPlayer += MakeMove;
            player2.MoveHandlerPlayer += MakeMove;

            if (CurrentPlayer is Computer)
                CurrentPlayer.Move(Board);

        }
        /// <summary>
        /// очищает доску
        /// </summary>
        public void NewGAme()
        {
            Board = new Board();
            GameOver = false;
            CurrentPlayer = playersList.First();
        }
        /// <summary>
        /// проверяет завершенность игры
        /// </summary>
        /// <returns></returns>
        private bool IsGameOver()
        {
            if (Board.CheckWinner(CurrentPlayer.PlayerContent))
                return true;
            if (Board.IsMove())
                return true;
            return false;
        }
        /// <summary>
        /// внесит ход игрока на игровое поле
        /// </summary>
        /// <param name="m"></param>
        /// <param name="p"></param>
        public void MakeMove(Move m, Player p)
        {
            if (CurrentPlayer == p)
            {
                if (Board.Move(m.Position, m.Content))
                {
                    Moves.Add(m);
                    if (!(GameOver = IsGameOver()))
                    {
                        ChooseNextGamer();
                    }
                    else
                    {
                        AddToDB();
                        Moves.Clear();
                    }
                }
            }
        }
        /// <summary>
        /// добавляет игру в БД
        /// </summary>
        private void AddToDB()
        {
            List<Entity.Move> list = new List<Entity.Move>();
            Moves.ForEach(m => list.Add(ConvertToEnity.ConvertMove(m)));
            db.Add(new Entity.Game {
                Winner = Board.CheckWinner(CurrentPlayer.PlayerContent)? (int)CurrentPlayer.PlayerContent : (int)Board.StateCell.Empty,
                MoveList = list
            });
        }
        /// <summary>
        /// передает ход следующему игроку
        /// </summary>
        private void ChooseNextGamer() => CurrentPlayer = playersList.Where(m => m != CurrentPlayer).Single();

    }
}
