﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToe.Models.TicTacToeGame.Users
{
    public class Human : Player
    {
        public Human(string name, Board.StateCell p) : base(name, p)
        {
        }

        public override void Move(Board gameBoard)
        {
            MadeMove();
        }
        /// <summary>
        /// ход человека
        /// </summary>
        /// <param name="position">ячейка хода</param>
        public void ChoseCell(int position)
        {
            CurrentMove = new Move(position, this.PlayerContent);
        }
    }
}
