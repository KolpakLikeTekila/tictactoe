﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TicTacToe.Models.TicTacToeGame.AI;

namespace TicTacToe.Models.TicTacToeGame.Users
{
    public class Computer : Player
    {

        private int Depth { get; set; }

        public Computer(string name, Board.StateCell p, int depth = 3) : base(name, p)
        {
            Depth = depth;
        }

        public override void Move(Board board)
        {
            AIMove Ai = new AIMove(PlayerContent);
            CurrentMove = Ai.MiniMax(board, Depth).Move;
            MadeMove();
        }
    }
}
