﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToe.Models.TicTacToeGame.Users
{
    public abstract class Player
    {
        
        public delegate void MoveHandler(Move m, Player p);
        public event MoveHandler MoveHandlerPlayer;
        public Board.StateCell PlayerContent { get; private set; }
        public string Name { get; private set; }
        protected Move CurrentMove { get; set; }

        public Player(string name, Board.StateCell p)
        {
            Name = name;
            PlayerContent = p;
        }
        /// <summary>
        /// ход игрока
        /// </summary>
        /// <param name="obj"></param>
        public abstract void Move(Board obj);
        /// <summary>
        /// изменяет состояние игры
        /// </summary>
        protected void MadeMove()
        {
            MoveHandlerPlayer(CurrentMove, this);
        }
    }
}
