﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToe.Models.TicTacToeGame.AI
{
    public class Score
    {
        public Move Move { get; private set; }
        public int Eval { get; private set; }
        public List<Score> MoveList { get; private set; } = new List<Score>(); 

        public Score(Move move, int score)
        {
            Move = move;
            Eval = score;
        }

        public Score(Move move, int score, List<Score> moveList):this(move,score)
        {
            MoveList = moveList;
        }
    }
}
