﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToe.Models.TicTacToeGame.AI
{
    public class AIMove
    {
        const int EVALUATION = 5;
        Board.StateCell OppContent { get; set; }
        Board.StateCell PlayerContent { get; set; }

        public AIMove(Board.StateCell playerContent)
        {
            PlayerContent = playerContent;
            OppContent = PlayerContent == Board.StateCell.X ? Board.StateCell.O : Board.StateCell.X;
        }
        /// <summary>
        /// стратегия АИ
        /// </summary>
        /// <param name="board">игровая доска</param>
        /// <param name="depth">глубина прогноза</param>
        /// <param name="turn">возможность своего хода</param>
        /// <param name="movePlayer">прогноз хода</param>
        /// <returns>лучший ход</returns>
        public Score MiniMax(Board board,  int depth, bool turn = true, Move movePlayer = null)
        {
            List<Score> scoreMovesList = new List<Score>();
            int scoreBest = Evaluate(board);
            Board.StateCell playerContent = turn ? PlayerContent : OppContent;

            List<Move> movesList = GetAllMovesList(board, playerContent);


            if (movesList.Count() == 0 || depth == 0|| scoreBest <0)
            {
                return new Score(movePlayer, Evaluate(board));
            }
            else
            {
                foreach (Move move in movesList)
                {
                    Board boardClone = (Board)board.Clone();
                    boardClone.Move(move.Position, playerContent);
                    Score scoreMove = MiniMax(boardClone, depth - 1, !turn, move);
                    scoreMovesList.Add(scoreMove);
                    boardClone.RevMove(move.Position);
                }
                if (turn)
                {
                    scoreBest = scoreMovesList.Max(list => list.Eval);
                }
                else
                {
                    scoreBest = scoreMovesList.Min(list => list.Eval);
                }
            }
            if (movePlayer == null)
                return RandomizeList(scoreMovesList.Where(m => m.Eval == scoreBest).ToList()).First();

            return new Score(movePlayer, scoreBest, scoreMovesList);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="board">игровое поле</param>
        /// <param name="content">фигура ходящего</param>
        /// <returns>все возможные ходы</returns>
        private List<Move> GetAllMovesList(Board board, Board.StateCell content)
        {
            List<Move> nextMoves = new List<Move>();
            for (int i = 0; i < board.Size; ++i)
            {
                if (board.GetBoardCell(i) == (int)Board.StateCell.Empty)
                {
                    nextMoves.Add(new Move(i, content));
                }
            }
            return nextMoves;
        }
        /// <summary>
        /// Перемешивает в случайном порядке список возможныйх ходов
        /// </summary>
        /// <param name="list">список возможныйх ходов</param>
        /// <returns>список возможныйх ходов</returns>
        private List<Score> RandomizeList(List<Score> list)
        {
            Random random = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                Score value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }
        /// <summary>
        /// оценивает риск хода
        /// </summary>
        /// <param name="board">игровое поле</param>
        /// <returns>оценка хода</returns>
        private int Evaluate(Board board)
        {
            bool oppWin;
            bool playerWin;
            oppWin = board.CheckWinner(OppContent);
            playerWin = board.CheckWinner(PlayerContent);
            if(playerWin)
                return EVALUATION;
            else if (oppWin)
                return -EVALUATION;
            return 0;
        }
    }
}