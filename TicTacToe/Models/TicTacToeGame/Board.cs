﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace TicTacToe.Models.TicTacToeGame
{
    public class Board :ICloneable
    {
        public enum StateCell
        {
            Empty,
            X,
            O
        };
        public Queue<int> winMove = null;
        const int LENGHT_FIELD = 3;
        public int Size { get; } = LENGHT_FIELD* LENGHT_FIELD;
        public int LenghtField { get; } = LENGHT_FIELD;

        private StateCell[] board;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index">ячейка поля</param>
        /// <returns>фигура на выбранной ячейке</returns>
        public StateCell GetBoardCell(int index)
        {
            if (index >= 0 && index < Size)
                return board[index];
            else
                throw new IndexOutOfRangeException();
        }

        public Board() {
            board = new StateCell[Size];
        }
        /// <summary>
        /// проверяет на возможность хода
        /// </summary>
        /// <param name="position">номер ячейки хода</param>
        /// <returns></returns>
        private bool IsValidSquare(int position)
        {
                if (board[position] == StateCell.Empty && position>=0)
                    return true;
            return false;
        }
        /// <summary>
        /// заносит ход на игровое поле
        /// </summary>
        /// <param name="position">номер ячейки хода</param>
        /// <param name="cell">фигура</param>
        /// <returns></returns>
        public bool Move(int position, StateCell cell)
        {
            if (IsValidSquare(position))
            {
                board[position] = cell;
                return true;
            }
            return false;
        }
        /// <summary>
        /// чистит ячейку
        /// </summary>
        /// <param name="position">номер ячейки</param>
        public void RevMove(int position) => board[position] = StateCell.Empty;

        public bool IsMove()
        {
           foreach(StateCell content in board)
                if(content == StateCell.Empty)
                    return false;
            return true;
        }

        /// <summary>
        /// проверяет на победителя
        /// </summary>
        /// <param name="cell">фигура игрока</param>
        /// <returns></returns>
        public bool CheckWinner(StateCell cell)
        {
            bool eq = true;
            winMove = new Queue<int>();
            //проверка по строкам
            for (int i =0; i< LenghtField; i++)
            {
                eq = true;
                for (int j = 0; j< LenghtField; j++)
                {
                    winMove.Enqueue(i * LenghtField + j);
                    if (board[i * LenghtField + j] != cell)
                    {
                        eq = false;
                        winMove.Clear();
                        break;
                    }
                }
                if (eq)
                {
                    return true;
                }
            }

            //проверка по столбцам
            for (int i = 0; i < LenghtField; i++)
            {
                eq = true;
                for (int j = 0; j < LenghtField; j++)
                {
                    winMove.Enqueue(i + LenghtField * j);
                    if (board[i + LenghtField * j] != cell)
                    {
                        eq = false;
                        winMove.Clear();
                        break;
                    }
                }
                if (eq)
                {
                    return true;
                }
            }

            //проверка по диагонали
            eq = true;
            for (int i = 0; i < Size; i+= LenghtField + 1)
            {
                winMove.Enqueue(i);
                if (board[i]!= cell)
                {
                    winMove.Clear();
                    eq = false;
                    break;
                }
            }
            if (eq)
            {
                return true;
            }

            eq = true;
            for (int i = LenghtField - 1; i <= Size- LenghtField; i += LenghtField - 1)
            {
                winMove.Enqueue(i);
                if (board[i] != cell)
                {
                    winMove.Clear();
                    eq = false;
                    break;
                }
            }
            if (eq)
            {
                return true;
            }

            return false;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
