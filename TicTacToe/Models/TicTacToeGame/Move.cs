﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicTacToe.Models.TicTacToeGame
{
    public class Move
    {
        public int Position { get; private set; }
        public Board.StateCell Content { get; private set; }

        public Move(int position, Board.StateCell cell)
        {
            Position = position;
            Content = cell;
        }
    }
}
