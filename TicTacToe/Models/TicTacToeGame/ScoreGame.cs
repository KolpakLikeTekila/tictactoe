﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TicTacToe.Models.TicTacToeGame
{
    class ScoreGame
    {
        public int ScoreX { get; set; } = 0;
        public int ScoreY { get; set; } = 0;
        public int ScoreDraw { get; set; } = 0;
        public string Title { get; set; }
        public bool TurnHuman { get; set; } = true;
    }
}