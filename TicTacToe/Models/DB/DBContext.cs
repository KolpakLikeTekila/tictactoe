﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TicTacToe.Models.Entity;

namespace TicTacToe.Models.DB
{
    public class DBContext: DbContext
    {
        public DBContext(): base("TicTacToe")
        {
        }

        public DbSet<Game> Games { get; set; }
        public DbSet<Move> Moves { get; set; }
    }
}