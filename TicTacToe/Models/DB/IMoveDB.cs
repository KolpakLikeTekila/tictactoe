﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Models.Entity;

namespace TicTacToe.Models.DB
{
    interface IMoveDB
    {
        IEnumerable<Move> List();
        Move Get(int? id);
    }
}
