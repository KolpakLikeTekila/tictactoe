﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicTacToe.Models.Entity;

namespace TicTacToe.Models.DB
{
    public class DBRepos : IGameDB, IMoveDB
    {

        public Game Get(int? id)
        {
            using (DBContext db = new DBContext())
            {
                return db.Games.Find(id);
            }
        }

        public IEnumerable<Game> List()
        {
            using (DBContext db = new DBContext())
            {
                return db.Games;
            }
        }

        public void Add(Game game)
        {
            using (DBContext db = new DBContext())
            {
                db.Games.Add(game);
                foreach (Move mv in game.MoveList)
                    db.Moves.Add(mv);
                db.SaveChanges();
            }
        }

        Move IMoveDB.Get(int? id)
        {
            using (DBContext db = new DBContext())
            {
                return db.Moves.Find(id);
            }
        }

        IEnumerable<Move> IMoveDB.List()
        {
            using (DBContext db = new DBContext())
            {
                return db.Moves;
            }
        }
    }
}