﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TicTacToe.Models.TicTacToeGame;

namespace TicTacToe.Models.DB
{
    static public class ConvertToEnity
    {/// <summary>
    /// 
    /// </summary>
    /// <param name="cl">класс</param>
    /// <returns>сущнсть</returns>
        static public Entity.Move ConvertMove(Move cl)
        {
            return new Entity.Move() { Position = cl.Position, Content = (int)cl.Content };
        }
    }
}