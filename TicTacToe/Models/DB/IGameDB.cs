﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Models.Entity;

namespace TicTacToe.Models.DB
{
    interface IGameDB
    {
        IEnumerable<Game> List();
        void Add(Game game);
        Game Get(int? id);
    }
}
