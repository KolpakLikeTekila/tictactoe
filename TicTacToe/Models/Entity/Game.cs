﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToe.Models.Entity
{
    public class Game
    {
        public int Id { get; set; }
        public int Winner { get; set; }
        public IEnumerable<Move> MoveList { get; set; } = new List<Move>();
    }
}