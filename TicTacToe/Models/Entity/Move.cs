﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TicTacToe.Models.Entity
{
    public class Move
    {
        public int Id { get; set; }
        public int Position { get; set; }
        public int Content { get; set; }

        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}